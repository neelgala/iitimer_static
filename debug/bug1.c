/*
Program received signal SIGSEGV, Segmentation fault.
[Switching to Thread 0x7fffe3ca3700 (LWP 27596)]
0x0000000000407240 in calculateDelaySlew (rctree=0x2529188, tapCap=0x2584ef0, 
    numtaps=1, delayR=0x2584f30, slewR=0x2584f50, outCap=0x7fffe3ca2df0)
    at ./src/parser_netlist.c:948
948	            if(visited[neigh] == 0) // Process only child nodes

* Problem with the first wire itself
* The Wire doesn't seem to have been read in properly.
* For example, the tap nodename is ""
* Look into src/parser_netlist.c:<where the wire node is created and print out>
	The tap nodes are properly read
	Looks like valgrind will help
	Tried out on a different machine and compiled without any warnings and the
	code worked without error.
	So need to look into the warnings - The following code illustrates how it can be fixed
	DIDNOT FIX

*/
#include <stdio.h>
#include <pthread.h>
void* computeWireTData_p(void *args)
{
    int tid = *(int*)args;
    fprintf(stderr,"tid = %d",tid);
}

int main(){

    	pthread_t thr;
	int i = 1;
	int rcc = pthread_create(&thr, NULL, computeWireTData_p, (void *)&i);
}
