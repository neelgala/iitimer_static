/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

// This file takes the result calculated by engine.c and prints it out in the required format
/*
Specifications:
---------------
The result of the timing analysis of a circuit, which should be produced in the specified output file,
will consist of two consecutive sets of lines, which will list projected values of parametric timing
quantities like arrival times, slews and slacks.

[DONE] <--    The first set of lines will consist of a list of lines, starting with the at keyword, one per primary
output node, containing the node name followed by the corresponding early and late projected
arrival times, as well as the early and late projected slews.

[DONE] <-- This list of nodes should be ordered
lexicographically in ascending order (using the ASCII code ordering sequence).

[NOT DONE] <--    The second set of lines will consist of a list of lines, starting with the slack keyword, one per
required arrival time constraint, followed by the node name and either the early or late keyword,
indicating either early or late mode, respectively. Finally, the value of the projected slack should
be printed.

[NOT DONE] <--This list of nodes should also be ordered lexicographically in ascending order (using
the ASCII code ordering sequence).

[NOT DONE : Currently late/early slack is not distinguished] <--The early slack will appear before the late slack, for nodes
where both exist.

[DONE in the engine] <--Slacks are induced either by explicit required arrival time constraints defined in
the netlist file,

[DONE in the engine] <--or by implicit setup and hold constraints, that must be considered for every flip-flop
in the circuit.

[DONE in the engine] <--The slacks (early, late, or both) should therefore be reported for all the nodes in the
fanin cone of any given node for which an explicit or implicit required arrival time constraint must
be considered.

[DONE] <---    The projection technique used in the output should be based on the value of the shell environ-
mental variable $TAU_PROJECTION which could be any of MEAN ONLY, SIGMA ONLY, or WORST CASE. If
the variable is not set, the default projection should be WORST CASE. While the two former projec-
tion techniques will be primarily used for testing and debugging, final evaluations would be based
on WORST CASE projection.

[DONE] <---  All numerical results will be given in seconds and printed in scientific notation, with 5 decimal
places (e.g. 1.23456e-10).

[DONE] <--- All keywords and variable fields should be separated by a white space.

References used:
    * http://crasseux.com/books/ctutorial/Environment-variables.html - Google "read shell environment variable in C"
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h> // fabs
#include <string.h>
#include <assert.h>

#include "output.h"
#include "objects.h" // vSink,
#include "pins.h" //NODE_TIMING_DATA, instanceIPin
#include "main.h" // To access the environment variables
#include "lexicographic.h"

#define SQR(x) (x)*(x)
#define SQRT(x) sqrt(x)

#define PROJ_WORST_CASE 0
#define PROJ_SIGMA_ONLY 1
#define PROJ_MEAN_ONLY 2

int projection; // 0 = WORST_CASE, 1 = MEAN_ONLY, 2 = SIGMA_ONLY

void SUB(float z[8], float x[8], float y[8]);

void printSlack(char* nodename, NODE_TIMING_DATA *tData)
{
    // print early slack first
    // fprintf (stderr, "\n Proper projection not used for slack");

    float fall[8], rise[8];
    float slackfall, slackrise;

    if ( tData->slack_early == 1)
    {
        SUB(fall, tData->atFallEarly, tData->ratFallEarly);
        SUB(rise, tData->atRiseEarly, tData->ratRiseEarly);

        if (projection == PROJ_WORST_CASE)
        {
            slackfall = fall[0] - 3 * SQRT ( SQR(fall[1]) + SQR(fall[2]) + SQR(fall[3]) + SQR(fall[4]) + SQR(fall[5]) ) - 3 * fabs(fall[6]) - 3 * fabs(fall[7]);
            slackrise = rise[0] - 3 * SQRT ( SQR(rise[1]) + SQR(rise[2]) + SQR(rise[3]) + SQR(rise[4]) + SQR(rise[5]) ) - 3 * fabs(rise[6]) - 3 * fabs(rise[7]);
        }
        else if (projection == PROJ_MEAN_ONLY)
        {
            slackfall = fall[0];
            slackrise = rise[0];
        }
        else if (projection == PROJ_SIGMA_ONLY)
        {
            slackfall = SQRT ( SQR(fall[1]) + SQR(fall[2]) + SQR(fall[3]) + SQR(fall[4]) + SQR(fall[5]) +SQR(fall[6]) + SQR(fall[7]) );
            slackrise = SQRT ( SQR(rise[1]) + SQR(rise[2]) + SQR(rise[3]) + SQR(rise[4]) + SQR(rise[5]) +SQR(rise[6]) + SQR(rise[7]) );
        }

	if(slackfall < slackrise)
	        fprintf(out_file, "slack %s early %.5e \n",nodename, slackfall);
	else
        	fprintf(out_file, "slack %s early %.5e \n",nodename, slackrise);
    }

    if ( tData->slack_late == 1)
    {
        SUB(fall, tData->ratFallLate, tData->atFallLate);
        SUB(rise, tData->ratRiseLate, tData->atRiseLate);

        if (projection == PROJ_WORST_CASE)
        {
            slackfall = fall[0] - 3 * SQRT ( SQR(fall[1]) + SQR(fall[2]) + SQR(fall[3]) + SQR(fall[4]) + SQR(fall[5]) ) - 3 * fabs(fall[6]) - 3 * fabs(fall[7]);
            slackrise = rise[0] - 3 * SQRT ( SQR(rise[1]) + SQR(rise[2]) + SQR(rise[3]) + SQR(rise[4]) + SQR(rise[5]) ) - 3 * fabs(rise[6]) - 3 * fabs(rise[7]);
        }
        else if (projection == PROJ_MEAN_ONLY)
        {
            slackfall = fall[0];
            slackrise = rise[0];
        }
        else if (projection == PROJ_SIGMA_ONLY)
        {
            slackfall = SQRT ( SQR(fall[1]) + SQR(fall[2]) + SQR(fall[3]) + SQR(fall[4]) + SQR(fall[5]) +SQR(fall[6]) + SQR(fall[7]) );
            slackrise = SQRT ( SQR(rise[1]) + SQR(rise[2]) + SQR(rise[3]) + SQR(rise[4]) + SQR(rise[5]) +SQR(rise[6]) + SQR(rise[7]) );
        }

	if(slackfall < slackrise)
	        fprintf(out_file, "slack %s late %.5e \n",nodename, slackfall);
	else
        	fprintf(out_file, "slack %s late %.5e \n",nodename, slackrise);
    }

}
void printSlack_iop(instanceOPin *iop)
{
    printSlack(iop->nodename, &iop->tData);
}
void printSlack_iip(instanceIPin *iip)
{
    printSlack(iip->nodename, &iip->tData);
}

void toolOutput_at(void)
{

    int i;

    // Step 1
    // Sort the primary output nodes based on the steps discussed in
    // toolOutput():"Lexicographic ordering of nodes"
    // Here there is no need to create another array of pointers as vSink->ipin itself is the
    // required array

    // Step 1.1 : Create an indx array
    int *PO_indx = (int*)malloc(sizeof(int)*(vSink->numIPins));

    for(i = 0; i < vSink->numIPins; i++)
    {
        PO_indx[i] = i;
    }

    // Step 1.2 :
    // Sort the array of indices based on the ordering of the nodenames
    quickSort_ipin( vSink->ipin, PO_indx, 0, vSink->numIPins - 1);

    ///// DEBUG ////
    // fprintf(stderr, "\n == Sorted Array of POs ==");
    // for (i = 0; i < vSink->numIPins; i++)
    // {
    //     fprintf(stderr, "\n %s",vSink->ipin[PO_indx[i]]->nodename);
    // }

    // Sorting done.

    // Step 2 :
    // Print the arrival time at each input pin of the vSink
    // using the indx found above to maintain the lexicographic ordering.
    NODE_TIMING_DATA *tData;
    instanceIPin *PO;

    float* vector;
    float atearlyfall, atearlyrise, atlatefall, atlaterise, slewearlyfall, slewearlyrise,slewlatefall,slewlaterise;

    for(i = 0; i < vSink->numIPins; i++)
    {
        PO = vSink->ipin[PO_indx[i]];

        tData = &(PO->tData);

        if (projection == PROJ_WORST_CASE) // Worst case
        {
            // arrival times
            vector = tData->atFallEarly;
            atearlyfall = vector[0] - 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) - 3 * fabs(vector[6]) - 3 * fabs(vector[7]);

            vector = tData->atRiseEarly;
            atearlyrise = vector[0] - 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) - 3 * fabs(vector[6]) - 3 * fabs(vector[7]);

            vector = tData->atFallLate;
            atlatefall = vector[0] + 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) + 3 * fabs(vector[6]) + 3 * fabs(vector[7]);

            vector = tData->atRiseLate;
            atlaterise = vector[0] + 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) + 3 * fabs(vector[6]) + 3 * fabs(vector[7]);

            // slews
            vector = tData->slewFallEarly;
            slewearlyfall = vector[0] - 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) - 3 * fabs(vector[6]) - 3 * fabs(vector[7]);

            vector = tData->slewRiseEarly;
            slewearlyrise = vector[0] - 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) - 3 * fabs(vector[6]) - 3 * fabs(vector[7]);

            vector = tData->slewFallLate;
            slewlatefall = vector[0] + 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) + 3 * fabs(vector[6]) + 3 * fabs(vector[7]);

            vector = tData->slewRiseLate;
            slewlaterise = vector[0] + 3 * SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) ) + 3 * fabs(vector[6]) + 3 * fabs(vector[7]);

        }
        else if (projection == PROJ_MEAN_ONLY) // Mean
        {
            vector = tData->atFallEarly;
            atearlyfall = vector[0];

            vector = tData->atRiseEarly;
            atearlyrise = vector[0];

            vector = tData->atFallLate;
            atlatefall = vector[0] ;

            vector = tData->atRiseLate;
            atlaterise = vector[0] ;

            // slews
            vector = tData->slewFallEarly;
            slewearlyfall = vector[0];

            vector = tData->slewRiseEarly;
            slewearlyrise = vector[0];

            vector = tData->slewFallLate;
            slewlatefall = vector[0] ;

            vector = tData->slewRiseLate;
            slewlaterise = vector[0] ;
        }
        else if (projection == PROJ_SIGMA_ONLY)
        {
            vector = tData->atFallEarly;
            atearlyfall = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );

            vector = tData->atRiseEarly;
            atearlyrise = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );

            vector = tData->atFallLate;
            atlatefall = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );

            vector = tData->atRiseLate;
            atlaterise = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );

            // slews
            vector = tData->slewFallEarly;
            slewearlyfall = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );

            vector = tData->slewRiseEarly;
            slewearlyrise = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );

            vector = tData->slewFallLate;
            slewlatefall = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );

            vector = tData->slewRiseLate;
            slewlaterise = SQRT ( SQR(vector[1]) + SQR(vector[2]) + SQR(vector[3]) + SQR(vector[4]) + SQR(vector[5]) + SQR(vector[6]) + SQR(vector[7]) );
        }
        else if (projection == PROJ_MEAN_ONLY) // Mean
        {
            vector = tData->atFallEarly;
            atearlyfall = vector[0];

            vector = tData->atRiseEarly;
            atearlyrise = vector[0];

            vector = tData->atFallLate;
            atlatefall = vector[0] ;
        }

        fprintf(out_file, "at %s %.5e %.5e %.5e %.5e %.5e %.5e %.5e %.5e\n", PO->nodename,
                atearlyfall,atearlyrise,atlatefall,atlaterise,slewearlyfall,slewearlyrise,slewlatefall,slewlaterise);

    }

}
/*
void print_WNS_slack(void){
	// Traverse the list and pick out iop's corresponding to primary output
	// Maintain the worst negative slack of iops found so far

    instanceOPin *IOPNode = iopList.head;
	float WNS_early,WNS_late;

	// Initialize WNS with the slack of the first pin in the linked list
	float fall, rise;
        SUB(fall, IOPNode->tData->atFallEarly,IOPNode->tData->ratFallEarly);
        SUB(rise, IOPNode->tData->atRiseEarly,IOPNode->tData->ratRiseEarly);
	if (fall > rise) WNS_early = rise; else WNS_early = fall;
        
	SUB(fall, IOPNode->tData->ratFallLate,IOPNode->tData->atFallLate);
        SUB(rise, IOPNode->tData->ratRiseLate,IOPNode->tData->atRiseLate);
	if (fall > rise) WNS_late = rise; else WNS_late = fall;

    while(IOPNode != NULL)
    {
        if (IOPNode->tData.slack_early == 1 ) //| IOPNode->tData.slack_late == 1 )
        {
		// Check if the pin is a primary output
		if (IOPNode->wp == NULL && IOPNode->iip == NULL){
        		SUB(fall, IOPNode->tData->atFallEarly,IOPNode->tData->ratFallEarly);
	        	SUB(rise, IOPNode->tData->atRiseEarly,IOPNode->tData->ratRiseEarly);
			
			if(WNS_early>fall)
				WNS_early=fall;
			if(WNS_early>rise)
				WNS_early=rise;
            	//fprintf (out_file, "\nslack IOP %s", IOPNode->nodename);
		}
        }
        else if (IOPNode->tData.slack_late == 1 ) //| IOPNode->tData.slack_late == 1 )
        {
		// Check if the pin is a primary output
		if (IOPNode->wp == NULL && IOPNode->iip == NULL){
        		SUB(fall, IOPNode->tData->ratFallLate,IOPNode->tData->atFallLate);
	        	SUB(rise, IOPNode->tData->ratRiseLate,IOPNode->tData->atRiseLate);
			
			if(WNS_late>fall)
				WNS_late=fall;
			if(WNS_late>rise)
				WNS_late=rise;
            	//fprintf (out_file, "\nslack IOP %s", IOPNode->nodename);
		}
        }

        IOPNode = IOPNode->next;
    }


}
*/
void get_TNS(void){
	instanceIPin **arrPO = vSink->ipin;

	float WNS_early,WNS_late;
	float TNS_early=0.0;
	float TNS_late=0.0;

	// Initialize WNS with the slack of the first pin in the linked list
    	float fall[8], rise[8];
	char *nodename_early;
	char *nodename_late;

        SUB(fall, arrPO[0]->tData.atFallEarly,arrPO[0]->tData.ratFallEarly);
        SUB(rise, arrPO[0]->tData.atRiseEarly,arrPO[0]->tData.ratRiseEarly);
	if(rise[0]<0 && rise[0]<fall[0])
		TNS_early=TNS_early+rise[0];
	else if(fall[0]<0 && fall[0]<rise[0])
		TNS_early=TNS_early+fall[0];
        
	SUB(fall, arrPO[0]->tData.ratFallLate,arrPO[0]->tData.atFallLate);
        SUB(rise, arrPO[0]->tData.ratRiseLate,arrPO[0]->tData.atRiseLate);
	if(rise[0]<0 && rise[0]<fall[0])
		TNS_late=TNS_late+rise[0];
	else if(fall[0]<0 && fall[0]<rise[0])
		TNS_late=TNS_late+fall[0];


	int i;
	for (i = 1; i < vSink->numIPins; i++){
		SUB(fall, arrPO[i]->tData.atFallEarly,arrPO[i]->tData.ratFallEarly);
		SUB(rise, arrPO[i]->tData.atRiseEarly,arrPO[i]->tData.ratRiseEarly);

			if(rise[0]<0 && rise[0]<fall[0])
				TNS_early=TNS_early+rise[0];
			else if(fall[0]<0 && fall[0]<rise[0])
				TNS_early=TNS_early+fall[0];

//			fprintf(stdout,"Node name analysed = %s \n",arrPO[i]->nodename);
//			if(WNS_early>fall[0]){
//				WNS_early=fall[0];
//				nodename_early=arrPO[i]->nodename;
//			}
//			if(WNS_early>rise[0]){
//				WNS_early=rise[0];
//				nodename_early=arrPO[i]->nodename;
//			}

		SUB(fall, arrPO[i]->tData.ratFallLate,arrPO[i]->tData.atFallLate);
		SUB(rise, arrPO[i]->tData.ratRiseLate,arrPO[i]->tData.atRiseLate);
			if(rise[0]<0 && rise[0]<fall[0])
				TNS_late=TNS_late+rise[0];
			else if(fall[0]<0 && fall[0]<rise[0])
				TNS_late=TNS_late+fall[0];
//			if(WNS_late>fall[0]){
//				WNS_late=fall[0];
//				nodename_late=arrPO[i]->nodename;
//			}
//			if(WNS_late>rise[0]){
//				WNS_late=rise[0];
//				nodename_late=arrPO[i]->nodename;
//			}
	}
//	if(WNS_early>=0){ WNS_early=0; nodename_early=NULL; TNS_early=0;}
//	if(WNS_late>=0){ WNS_late=0; nodename_late=NULL; TNS_late=0;}
	fprintf(stdout,"  TNS_early = %.5e, TNS_late = %.5e\n",TNS_early, TNS_late);
}
void get_WNS(void){
	instanceIPin **arrPO = vSink->ipin;

	float WNS_early,WNS_late;

	// Initialize WNS with the slack of the first pin in the linked list
    	float fall[8], rise[8];
	char *nodename_early;
	char *nodename_late;

        SUB(fall, arrPO[0]->tData.atFallEarly,arrPO[0]->tData.ratFallEarly);
        SUB(rise, arrPO[0]->tData.atRiseEarly,arrPO[0]->tData.ratRiseEarly);
	if (fall[0] > rise[0]) WNS_early = rise[0]; else WNS_early = fall[0];
	nodename_early=arrPO[0]->nodename;
        
	SUB(fall, arrPO[0]->tData.ratFallLate,arrPO[0]->tData.atFallLate);
        SUB(rise, arrPO[0]->tData.ratRiseLate,arrPO[0]->tData.atRiseLate);
	if (fall[0] > rise[0]) WNS_late = rise[0]; else WNS_late = fall[0];
	nodename_late=arrPO[0]->nodename;


	int i;
	for (i = 1; i < vSink->numIPins; i++){
		SUB(fall, arrPO[i]->tData.atFallEarly,arrPO[i]->tData.ratFallEarly);
		SUB(rise, arrPO[i]->tData.atRiseEarly,arrPO[i]->tData.ratRiseEarly);
//			fprintf(stdout,"Node name analysed = %s \n",arrPO[i]->nodename);
			if(WNS_early>fall[0]){
				WNS_early=fall[0];
				nodename_early=arrPO[i]->nodename;
			}
			if(WNS_early>rise[0]){
				WNS_early=rise[0];
				nodename_early=arrPO[i]->nodename;
			}

		SUB(fall, arrPO[i]->tData.ratFallLate,arrPO[i]->tData.atFallLate);
		SUB(rise, arrPO[i]->tData.ratRiseLate,arrPO[i]->tData.atRiseLate);
			if(WNS_late>fall[0]){
				WNS_late=fall[0];
				nodename_late=arrPO[i]->nodename;
			}
			if(WNS_late>rise[0]){
				WNS_late=rise[0];
				nodename_late=arrPO[i]->nodename;
			}
	}
	if(WNS_early>=0){ WNS_early=0; nodename_early=NULL;}
	if(WNS_late>=0){ WNS_late=0; nodename_late=NULL;}
    fprintf(stdout,"  Early Node is = %s, Late Node = %s\n",nodename_early,nodename_late); 
 	fprintf(stdout,"  WNS_early = %.5e, WNS_late = %.5e\n",WNS_early, WNS_late);
//	fprintf(stdout,"\n WNS_early = %.5e, WNS_late = %.5e Early Node is =%s Late Node = %s\n",WNS_early, WNS_late,nodename_early,nodename_late);
}
void get_slack_wiretap(char *nodename_wire){

    wireTap *WTnode = wtList.head;
    float fall[8], rise[8];

    float node_early,node_late;
    int i = 0;
    while(WTnode != NULL)
    {
//	fprintf(stdout,"Parsing thru wire tap : %s \n",WTnode->nodename);
        if (strcmp(WTnode->nodename,nodename_wire) == 0)
        {
		SUB(fall, WTnode->iip->tData.atFallEarly,WTnode->iip->tData.ratFallEarly);
		SUB(rise, WTnode->iip->tData.atRiseEarly,WTnode->iip->tData.ratRiseEarly);
	        if(fall[0]<rise[0]) node_early=fall[0]; else node_early=rise[0];	

		SUB(fall, WTnode->iip->tData.ratFallLate,WTnode->iip->tData.atFallLate);
		SUB(rise, WTnode->iip->tData.ratRiseLate,WTnode->iip->tData.atRiseLate);
	        if(fall[0]<rise[0]) node_late=fall[0]; else node_late=rise[0];	

		fprintf(stdout,"Slack at Wire Tap %s  Early = %.5e Late = %.5e \n",nodename_wire,node_early,node_late);	
		return;
            //// DEBUG ///
            //fprintf (out_file, "\nslack IOP %s", IOPNode->nodename);
        }
        WTnode = WTnode->next;
    }
//	fprintf(stdout,"wire tap name not found\n");
	return;
}
void get_slack_wireport(char *nodename_wire){

    wirePort *WPnode = wpList.head;
    float fall[8], rise[8];

    float node_early,node_late;
    int i = 0;
    while(WPnode != NULL)
    {
//	fprintf(stdout,"Parsing thru wire port : %s \n",WPnode->nodename);
        if (strcmp(WPnode->nodename,nodename_wire) == 0)
        {
		SUB(fall, WPnode->iop->tData.atFallEarly,WPnode->iop->tData.ratFallEarly);
		SUB(rise, WPnode->iop->tData.atRiseEarly,WPnode->iop->tData.ratRiseEarly);
	        if(fall[0]<rise[0]) node_early=fall[0]; else node_early=rise[0];	

		SUB(fall, WPnode->iop->tData.ratFallLate,WPnode->iop->tData.atFallLate);
		SUB(rise, WPnode->iop->tData.ratRiseLate,WPnode->iop->tData.atRiseLate);
	        if(fall[0]<rise[0]) node_late=fall[0]; else node_late=rise[0];	

		fprintf(stdout,"Slack at Wire Port %s  Early = %.5e Late = %.5e \n",nodename_wire,node_early,node_late);	
		return;
            //// DEBUG ///
            //fprintf (out_file, "\nslack IOP %s", IOPNode->nodename);
        }
        WPnode = WPnode->next;
    }
	//fprintf(stdout,"Node name not found\n");
	return;
	
}


void toolOutput_slack(void)
{
    // Step 3.1a:
    // ---------
    // Find out the opins for which slack is known and create an array of pointers
    // Assume a maximum of iopList.count nodes

    instanceOPin** iop_ptr_list = (instanceOPin**)malloc(sizeof(instanceOPin*)*(iopList.count));
    instanceOPin *IOPNode = iopList.head;

    int i = 0;
    while(IOPNode != NULL)
    {
        if (IOPNode->tData.slack_early == 1 || IOPNode->tData.slack_late == 1 )
        {
            iop_ptr_list[i++] = IOPNode;
            //// DEBUG ///
            //fprintf (out_file, "\nslack IOP %s", IOPNode->nodename);
        }
        IOPNode = IOPNode->next;
    }

    int num_opins_slack_known = i;

    // Step 3.2a:
    // ----------
    // Create an array of integers to be used for sorting
    int *iop_indx = (int*)malloc(sizeof(int)*(num_opins_slack_known));
    for ( i = 0; i < num_opins_slack_known; i++)
    {
        iop_indx[i] = i;
    }

    // Step 3.3a:
    // ----------
    //      Sort the array of indices based on the ordering of the nodenames
    quickSort_opin( iop_ptr_list, iop_indx, 0, num_opins_slack_known - 1);

    ///////// DEBUG /////////////
    // fprintf(stderr, "\n == Sorted Array ==");
    // for (i = 0; i < num_opins_slack_known; i++)
    // {
    //     fprintf(stderr, "\n\t%s",iop_ptr_list[iop_indx[i]]->nodename);
    // }

    // Similarly for ipins ....
    // ------------------------
    // Step 3.1b:
    // ---------
    // Find out the ipins for which slack is known and create an array of pointers
    // Assume a maximum of iipList.count nodes

    instanceIPin** iip_ptr_list = (instanceIPin**)malloc(sizeof(instanceIPin*)*(iipList.count));
    instanceIPin *IIPNode = iipList.head;

    i = 0;
    while(IIPNode != NULL)
    {
        if (IIPNode->tData.slack_early == 1 || IIPNode->tData.slack_late == 1 )
        {
            iip_ptr_list[i++] = IIPNode;
            //// DEBUG ////
            //fprintf (out_file, "\nslack IIP %s", IIPNode->nodename);
        }
        IIPNode = IIPNode->next;
    }

    int num_ipins_slack_known = i;

    // Step 3.2b:
    // ----------
    // Create an array of integers to be used for sorting
    int *iip_indx = (int*)malloc(sizeof(int)*(num_ipins_slack_known + 1));
    for ( i = 0; i < num_ipins_slack_known; i++)
    {
        iip_indx[i] = i;
    }

    // Step 3.3b:
    // ----------
    //      Sort the array of indices based on the ordering of the nodenames
    quickSort_ipin( iip_ptr_list, iip_indx, 0, num_ipins_slack_known - 1);

    ////// DEBUG ////////
    // fprintf(stderr, "\n == Sorted Array ==");
    // for (i = 0; i < num_ipins_slack_known; i++)
    // {
    //     fprintf(stderr, "\n\t%s",iip_ptr_list[iip_indx[i]]->nodename);
    // }

    // Step 3.4 :
    // ----------
    // Combine the two sorted lists into one preserving the lexicographic ordering and write it out
    // into the file.

    // Add a black line
    //fprintf(out_file, "\n");

    int pos_iop = 0, pos_iip = 0;
    int diff;
    while (1)
    {
        if ( ! ( pos_iop < num_opins_slack_known ) && ! (pos_iip < num_ipins_slack_known) )
        {
            break;
        }
        else if ( pos_iop < num_opins_slack_known && pos_iip < num_ipins_slack_known )
        {
            diff = strcmp (iop_ptr_list[iop_indx[pos_iop]]->nodename, iip_ptr_list[iip_indx[pos_iip]]->nodename);
        }
        else if ( pos_iop < num_opins_slack_known )
        {
            diff = -1;
        }
        else
        {
            diff = +1;
        }

        if ( diff == 0 )
        {
            // Note: The same node appearing both as a iop and a iip pin indicate a direct connection (i.e. without
            // a wire) between two gates. The timing data of either could be used.

            printSlack_iop(iop_ptr_list[iop_indx[pos_iop]]);

            pos_iop++;
            pos_iip++;
        }
        else if ( diff < 0 )
        {

            printSlack_iop(iop_ptr_list[iop_indx[pos_iop]]);

            pos_iop++;
        }
        else
        {

            printSlack_iip(iip_ptr_list[iip_indx[pos_iip]]);

            pos_iip++;
        }

    }

}
void toolOutput()
{
    int i;

    //fprintf(out_file,"\n ===== Results =====");
    //fprintf(out_file,"\n First set of line - Projected arrival times at the POs in the format: at <nodename> <early at> <late at> <early slew> <late slew> [ordered in lexicographical ordering of nodename] \n");

    // Step 1 :
    // ========
    // Determine the method of projection from the environment variable and set the global variable
    // "int projection" accordingly.
    char *TAU_PROJECTION = getenv("TAU_PROJECTION");

    if (TAU_PROJECTION == NULL)
    {
//        fprintf(stderr,"env variable TAU_PROJECTION is not set. Defaulting to PROJ_MEAN_ONLY.\n");
        projection = PROJ_MEAN_ONLY;
    }
    else if (strcmp(TAU_PROJECTION,"WORST_CASE") == 0)
    {
        fprintf(stderr,"Using WORST_CASE projection\n");
        projection = PROJ_WORST_CASE;
    }
    else if (strcmp(TAU_PROJECTION,"MEAN_ONLY") == 0)
    {
        fprintf(stderr,"Using MEAN_ONLY projection\n");
        projection = PROJ_MEAN_ONLY;
    }
    else if (strcmp(TAU_PROJECTION,"SIGMA_ONLY") == 0)
    {
        fprintf(stderr,"Using SIGMA_ONLY\n");
        projection = PROJ_SIGMA_ONLY;
    }
    else
    {
        fprintf(stderr,"Invalid value of env variable TAU_PROJECTION. Defaulting to WORST_CASE.\n");
        projection = PROJ_WORST_CASE;
    }

    // Step 2:
    // =======
    // Print the arrival times at the primary outputs in lexicographic order
    // toolOutput_at();

    // Step 3:
    // =======
    // Print (in lexicographic ordering of the nodenames) the slacks at
    // all nodes (i.e. ipin or opin) for which tData.slack is 1.
    //      3.1: To do this construct an array of pointers such that (ptr->x : iff x.tData.slack == 1)
    //      3.2,3.3: Sort the arrays based on the lexicographic ordering of the node names
    //      ==> Do this for ipins (3.xa), opins(3.xb)
    //      3.4: Print out the results by traversing both arrays simultaneously such that the overall lexicographic order
    //      is maintained.
    toolOutput_slack();
//    get_slack_wireport("nx33_1");
//    get_slack_wiretap("nx44");

    // fprintf(stderr,"\n Bye!\n");
}

