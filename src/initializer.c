
#include <stdio.h>
#include <stdlib.h>
#include <math.h> // fabs
#include <string.h>
#include <assert.h>

#include "output.h"
#include "objects.h" // vSink,
#include "pins.h" //NODE_TIMING_DATA, instanceIPin
#include "main.h" // To access the environment variables
#include "initializer.h"
#include "lexicographic.h"

void initialize (int argc, char **argv, char **envp)
{
	
    if (argc < 3)
    {
        printf("\n Usage: %s <cell library> <netlist> <output file> \n\n",argv[0]);
        return;
    }

    // > Export the 3 essential command line arguments to global variables
    cellFile = argv[1];
    netlistFile = argv[2];
    outFile = argv[3];
    //delayFile = argv[3];
    num_threads = 1;
    // fprintf(stderr, "Trying to run it with %d threads\n", num_threads);

    envVars = envp;

    out_file = fopen (outFile,"w");
    if (out_file == NULL)
    {
        printf ("\n Unable to open file %s",outFile);
        exit(-1);
    }
		return;
}
