/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _HASH_H_
#define _HASH_H_

typedef struct{
    unsigned int hash;
    struct _wireTap *node;
}wireTHash;

typedef struct{
    unsigned int hash;
    struct _wirePort *node;
}wirePHash;

void sortWTHash(int start, int end);
void sortWPHash(int start, int end);
struct _wirePort* searchWPNode(char* nodename);

struct _instanceIPin* searchIIPNode(char* nodename);
struct _wireTap* searchWTNode(char *nodename);

extern unsigned int *iipHashKey;
extern struct _instanceIPin **iipHashNode;
void createiipHash();

extern unsigned int *iopHashKey;
extern struct _instanceOPin **iopHashNode;
void createiopHash();

extern unsigned int *wpHashKey;
extern struct _wirePort **wpHashNode;
void createwpHash();

extern unsigned int *wtHashKey;
extern struct _wireTap **wtHashNode;
void createwtHash();

unsigned int hash( const char* s, unsigned int seed);

#endif
