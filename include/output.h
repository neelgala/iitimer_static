/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _OUTPUT_H_
#define _OUTPUT_H_

void parseNetlist(char *netlistFile);
void parseCellLLibrary(char *cellFile);
void timingAnalysis();
void toolOutput();
void get_slack_wiretap();
void get_slack_wireport();
void get_WNS();
void get_TNS();

#endif
