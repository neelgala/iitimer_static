/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _PARSER_NETLIST_H_
#define _PARSER_NETLIST_H_
#define VSOURCE -1
#define VSINK -2
//#define NUM_PARSER_THREADS 4

#include "pins.h"
#include "objects.h"
//#include <pthread.h>
//
//typedef struct
//{
//	// Unique identifier
//	int threadid;
//	
//	// Data
//	IIPList iipList, icpList, rat_known_node_List;
//	IOPList iopList, at_known_node_List, slew_known_node_List;
//	WPList wpList;
//	WTList wtList;
//	IList iList;
//	WList wList;
//
//	Instance* vSource;
//	Instance* vSink;
//	
//    char *memblk;
//    long memblk_size;
//    int skipped_wire_start;
//    
//    // File scope
//    long eof; // This thread should stop at this line unless a wire is being read in
//    long bof; // beginning of file
//    
//	// POSIX thread
//	pthread_t thread;
//    
//    int read_just_a_wire;
//
//} ParserThread;
void parseNetlist(char *netlistFile);
void print_PI_PO();

#endif
