#! /usr/bin/perl
use strict;
use warnings;

# TAU Contest 2013 library file format specifies sensitivity numbers
# to sources of variation.
# This script, resets those parameters so as to remove the effect of
# variations.
# Following are the changes made:
#	* metal <v1> <v2> <v3> 
#		<v2> = 1.0
#		<v3> = 1.0
#	* timing <v1> <v2> <v3> <v4 v5 ... v12> <v13 ... v21> <v22 ... v30> <v31 ... v39>
#		Set v7 ... v12 = 0.0
#		Set v16 .. v21 = 0.0
#		Set v25 .. v30 = 0.0
#		Set v34 .. v39 = 0.0
#	* preset <v1> <v2> <v3> <v4 v5 ... v12> <v13 ... v21>
#		Set v7 ... v12 = 0.0
#		Set v16 .. v21 = 0.0
#	
# Usage
# =====
# Input is taken from STDIN line by line and the output is given to the STDOUT
# 

while (<STDIN>)
{
	chomp $_;
	my @list = split /\s+/,$_;

	# remove any leading white space
	my $len = length $list[0];
	shift @list if ($len == 0);
	
	my $numfields = scalar @list;

	# check for each type and modify
	if ($list[0] eq "metal" and $numfields == 4)
	{
		$list[2] = "1.0";
		$list[3] = "1.0";
	}
	if ($list[0] eq "timing" and $numfields == 40)
	{
		for (my $i = 7; $i <= 12; $i++) {$list[$i] = "0.0";}
		for (my $i = 16; $i <= 21; $i++) {$list[$i] = "0.0";}
		for (my $i = 25; $i <= 30; $i++) {$list[$i] = "0.0";}
		for (my $i = 34; $i <= 39; $i++) {$list[$i] = "0.0";}
	}
	if ($list[0] eq "preset" and $numfields = 22)
	{
		for (my $i = 7; $i <= 12; $i++) {$list[$i] = "0.0";}
		for (my $i = 16; $i <= 21; $i++) {$list[$i] = "0.0";}
	}

	print join(" ",@list);
	print "\n";
}
