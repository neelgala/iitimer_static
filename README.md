ICCAD 2014 IITimer, IIT Madras
===============================

This is the source code for the utility that generates the output file
containing the timing arcs from the cell library and netlist files.

The timing arcs expected to be covered by the utility are:

* input to output delay of combinational elements
* clock to output delay of sequential elements
* port to tap delays of wires

**Note**

1. The converter does a statistical timing analysis of the circuit. Hence, to make it variation 'unaware' the cell library file has to be appropriately modified by zeroing out the sensitivity parameters.

2. Since the utility generates a new line for every timing arc in the circuit, there can be multiple lines referring to the same pin pair (but corresponding to different gates) and there is no way to distinguish which line belongs to which gate.

3. `vsink:po` is used to refer to a primary output pin (vsink = virtual sink)

4. Following APIs are avaiable for use
  1. get\_WNS() - Prints the worst negative slack of the netlist on the terminal.
  2. get\_TNS() - Prints the Total negative slack of the netlist on the terminal.
  3. get\_slack\_wiretap("name\_of\_wire\_tap") - Prints the slack on the particular Wire tap.
  4. get\_slack\_wireport("name\_of\_wire\_port") - Prints the slack on the particular Wire Port.
  5. parseNetlist(char \*netlistFile); - Parses the netlist file.
  6. parseCellLLibrary(char \*cellFile); - Parses the cell library file.
  7. timingAnalysis(); - Performs Timing Analysis
  8. toolOutput() - Prints the outputs in the output file.    
                                         

Running the code
================

    make
    ./bin/iitimer <cell library> <netlist> <output file>

    # If required zero out variation sensitivities from the original cell library
    # using make_variation_unaware.pl script
    cat testcase/tau2013_contest_library.v1.0.lb | perl ./scripts/make_variation_unaware.pl > testcase/tau2013_contest_library.v1.0.novar.lb
    
    # From the root directory run,
    ./bin/iitimer testcase/tau2013_contest_library.v1.0.novar.lb testcase/c3_slack.net testcase/c3_slack_output
    
    # The result (TNS,WNS) will be printed on the terminal itself. The slack details can be seen by 
    cat test/c3_slack_output

The cell library and netlist files can be obtained from the website of
the original contest:
https://sites.google.com/site/taucontest2013/resources

Utility scripts
===============
The scripts directory contains two perl scripts which are described below:

make_variation_unaware.pl
-------------------------

This can be used to zero out all the variation aware parameters in the tau2013 cell library.

resolve_direct_connection.pl
----------------------------

Certain benchmark netlists like `mem_ctrl.net` etc. have primary inputs that directly connect to 
gate inputs. The converter cannot handle such a direct connection. The workaround is to insert
a zero-delay wire. This script can be used to make the necessary changes to the netlist.

Steps to run

Find out the direct connections netname (Can be found from the warning message given by the converter). Say its 'x147076' as found here.

    $ ./bin/iitimer ~/Downloads/tau2013_contest_library.v2.0.novar.lb ~/Downloads/tau2013_contest_benchmarks.v2.0-3/mem_ctrl.net delayfile
    SSTA Timer Team T13_11, IIT Madras
    Warning!!: Direct connection (i.e. without a wire) between pins (x147076 -- x147076). Resolve by adding a zero-delay wire to the netlist.
    Aborted

Invoke the script like so:

    cat mem_ctrl.net | perl scripts/resolve_direct_connection.pl x147076 > mem_ctrl.modified.net

Use diff to ensure the changes are valid

    gvim -d mem_ctrl.net mem_ctrl.modified.net

`mem_ctrl.modified.net` can now be converted to 2014 format without a problem.

FAQs
====

1. The converter aborts with a `Warning!!: Direct connection ...` message ?

The netlist file needs to be modified. See the description under `resolve_direct_connection.pl` above for details.

License
=======

This code is released under the terms of a __________________ license.
Please see the file LICENSE for details.

- V. Sudharshan.
- Jobin Jacob Kavalam.
- Neel Gala.
- Shankar Balachandran.
- Nitin Chandrachoodan. (contact: nitin@ee.iitm.ac.in)

- - - - - - - - - - - - - - - - - - -
Initial public release: ### ##, ####
